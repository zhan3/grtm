#!/bin/env python

# Simulation study for beta error comparison
from numpy.random import dirichlet
from scipy.stats import poisson
from numpy.random import multinomial
from scipy.stats import bernoulli
import numpy as np
from scipy import sparse
import matplotlib.pylab as plt
import pandas as pd
import random
from scipy.spatial import distance
import rtm
import grtm
import sys, getopt

def logistic(x):
    return np.exp(x)/(1+np.exp(x))

def LDA_pre(X,filename):
    f = open(filename,'w')
    num_word = X.shape[1]
    num_doc = X.shape[0]
    X_sparse = sparse.csr_matrix(X)
    M = np.diff(X_sparse.indptr)
    output = ""
    for i in range(num_doc):
        M_ = M[i]
        output += str(M_)
        start = X_sparse.indptr[i]
        end = X_sparse.indptr[i+1]
        for iter_ in range(start,end):
            output += " %s:%s" %(X_sparse.indices[iter_],int(X_sparse.data[iter_]))
        output += "\n"
    f.writelines(output)
    f.close()
    return 1

def GenerateDocument(NUM_DOCUMENTS,NUM_TOPICS,NUM_WORDS,ALPHA,BETA,POISSON_RATE):
    #construct corpus
    corpus = dict()
    corpus_theta = dict()
    corpus_topic = dict()

    for d in range(NUM_DOCUMENTS):
        #choose theta
        Theta_d = dirichlet([ALPHA]*NUM_TOPICS,1)
        corpus_theta.update({d:Theta_d})
        #choose word count
        n_d = poisson.rvs(POISSON_RATE)
        word_list = []
        topic_list = []
        #for each word
        for wd in range(n_d):
            #draw a topic
            topic_v = multinomial(1,Theta_d[0])
            topic = np.where(topic_v)[0][0]
            #draw a word
            word = np.where(multinomial(1,BETA[topic,]))[0][0]
            word_list.append(word)
            topic_list.append(topic)
        corpus.update({d:word_list})
        corpus_topic.update({d:topic_list})
    #generate X for lda package
    X = np.zeros(shape=(NUM_DOCUMENTS,NUM_WORDS))
    for d,w in corpus.iteritems():
        for word in w:
            X[d,word] += 1
    
    return X,corpus_topic

def Distance(beta1,beta2,**args):
    dis = distance.cdist(beta1,beta2)
    error = 0
    for i in range(dis.shape[0]):
        index =  int(np.where(dis[i] == dis[i].min())[0])
        error += dis[i,index]
        dis = np.delete(dis, index, 1)
    return error

def main(argv):
    '''
        THis is a 1 - inter matching with added document shared by all users scheme
    '''
    opts, args = getopt.getopt(argv,"M:T:D:U:P:",["Method=","Num_Topic=","Num_Document=","Num_User=","Num_Add="])
    
    # Initial parameters
    NUM_WORDS = 1000
    NUM_TOPICS = 2
    PRIOR_BETA = 0.5
    ALPHA = 0.1
    NUM_DOCUMENTS = 200
    NUM_USER = 200
    POISSON_RATE = 30
    Num_Add = 0
    tol = 0.00001
    NSIMS = 250
    METHOD = -1
    
    # Reading into the argument
    for opt, arg in opts:
        if opt in ("-M","--Method"):
            if arg == "RTM":
                METHOD = 1
                # RUN RTM
            elif arg == "GRTM":
                # RUN GRTM
                METHOD = 2
        elif opt in ("-T","--Num_Topic"):
            NUM_TOPICS = np.int(arg)
        elif opt in ("-D","--Num_Document"):
            NUM_DOCUMENTS = np.int(arg)
        elif opt in ("-U","--Num_User"):
            NUM_USER = np.int(arg)
        elif opt in ("-P","--Num_Add"):
            Num_Add = np.int(arg)
        else:
            print "Extra parameters",opt

    # print out parameters for double check
    print "NUM_WORDS %s \n" %NUM_WORDS
    print "NUM_TOPICS %s \n" %NUM_TOPICS
    print "NUM_DOCUMENTS %s \n" %NUM_DOCUMENTS
    print "NUM_USER %s \n" %NUM_USER
    print "Num_Add %s \n" %Num_Add
    print "tol %s \n" %tol
    print "NSIMS %s \n" %NSIMS
    print "METHOD %s \n" %METHOD
    
    error = []
    # Start the simulation
    for i in range(NSIMS):
        
        # generate beta 
        BETA = dirichlet([PRIOR_BETA]*NUM_WORDS,NUM_TOPICS)

        # generate data
        X,corpus_topic = GenerateDocument(NUM_DOCUMENTS,NUM_TOPICS,NUM_WORDS,ALPHA,BETA,POISSON_RATE)

        # 1 - interger matching
        assert NUM_DOCUMENTS%NUM_USER == 0
        Mapping = np.diag([1]*NUM_USER)
        if NUM_DOCUMENTS/NUM_USER>1:
            for i in range(1,NUM_DOCUMENTS/NUM_USER):
                Mapping = np.hstack([Mapping,np.diag([1]*NUM_USER)])

        # add 
        assert NUM_DOCUMENTS%NUM_USER == 0
        add_NUM = Num_Add
        added_Doc,corpus_topic_new = GenerateDocument(add_NUM,NUM_TOPICS,NUM_WORDS,ALPHA,BETA,POISSON_RATE)
        Mapping = np.hstack([Mapping,np.ones(shape=(NUM_USER,add_NUM))])
        X = np.vstack([X,added_Doc])
        for key in corpus_topic_new:
            corpus_topic.update({(NUM_DOCUMENTS+key):corpus_topic_new[key]})

        # aggregate to user
        USER_LEVEL_X = np.zeros(shape=(NUM_USER,NUM_WORDS))
        for user in range(NUM_USER):
            USER_LEVEL_X[user,:] = X[np.where(Mapping[user] == 1)[0],:].sum(0)
        USER_LEVEL_X = pd.DataFrame(USER_LEVEL_X,dtype=int)   

        #Aggregate User level interest
        user_interest = dict()
        for u in range(NUM_USER):
            temp = []
            for d in np.where(Mapping[u,])[0]:
                temp_topic = corpus_topic[d]
                temp.extend(temp_topic)
            user_interest.update({u:np.histogram(temp,bins =  np.linspace(-0.5,NUM_TOPICS-0.5,num=NUM_TOPICS + 1))[0]/float(len(temp))})

        # construct user network with some prespecifice eta and v
        ETA = np.array([15.0]*NUM_TOPICS)
        V = -10.0
        edge_prob = np.zeros(shape=(NUM_USER,NUM_USER))
        for u in range(NUM_USER):
            for up in range(NUM_USER):
                edge_prob[u,up] = edge_prob[up,u] = logistic(ETA.T.dot(user_interest[u] * user_interest[up]) + V)

        #Generate links
        LINK = np.zeros(shape=(NUM_USER,NUM_USER),dtype=bool)
        for i in range(NUM_USER-1):
            for j in range(i+1,NUM_USER):
                LINK[i,j] = bernoulli.rvs(edge_prob[i,j])
        LINK = LINK + LINK.T

        # set into different methods
        if METHOD == 1:
            # RUN RTM
            doc_ids = []
            doc_cnt = []
            for x in np.array(USER_LEVEL_X):
                idx = np.where(x!=0)[0]
                doc_cnt.append(x[idx].tolist())
                doc_ids.append(idx.tolist())

            doc_links = []
            for l in LINK:
                doc_links.append(np.where(l!=0)[0].tolist())

            rtm_model = rtm.rtm(num_topic=NUM_TOPICS, num_doc=USER_LEVEL_X.shape[0], num_voca=USER_LEVEL_X.shape[1], doc_ids=doc_ids, 
                                doc_cnt=doc_cnt, doc_links=doc_links, rho=1)
            rtm_model.posterior_inference(tol)
            
            # Calculate error
            error.append(Distance(BETA,rtm_model.beta))


        elif METHOD == 2:
            # RUN GRTM
            doc_ids  = []
            doc_cnt = []
            for x in np.array(X):
                idx = np.where(x!=0)[0]
                doc_cnt.append(x[idx].tolist())
                doc_ids.append(idx.tolist())

            user_ids = []
            for u in range(NUM_USER):
                user_ids.append(np.where(Mapping[u,]!=0)[0].tolist())

            user_links = []
            for u in range(NUM_USER):
                user_links.append(np.where(LINK[u,]!=0)[0].tolist())

            grtm_model = grtm.grtm(num_topic=NUM_TOPICS, num_doc=X.shape[0], num_voca=X.shape[1], num_user = NUM_USER,doc_ids=doc_ids, 
                     doc_cnt=doc_cnt, user_ids = user_ids,user_links=user_links, rho=1)
            grtm_model.posterior_inference(tol)
    
            # Calculate error
            error.append(Distance(BETA,grtm_model.beta))
            
    # store error
    filename = str(METHOD)+'_'+str(NUM_TOPICS)+'_'+str(NUM_DOCUMENTS)+'_'+str(NUM_USER)+'_'+str(add_NUM)
    np.array(error).tofile(filename)


if __name__ == "__main__":
   main(sys.argv[1:])
