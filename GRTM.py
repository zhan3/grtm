#!/bin/env python


import numpy as np
from scipy.special import gammaln, psi
#from formatted_logger import formatted_logger

eps = 1e-20

#log = formatted_logger('RTM', 'info')

class grtm:
    '''
        Implementation of relational user interests model 
        Modified from the RTM python module by Dongwoo Kim
        Only implemented the exponential link probability function
    '''

    def __init__(self, num_topic, num_doc, num_voca, num_user,doc_ids, doc_cnt, user_ids,user_links, rho):
        
        '''
            parameter:
                doc_ids = list([unique words] * number of document)
                doc_cnt = list([word count for each unique word] * number of document)
                user_ids = list([owned documents] * number of users)
                user_links = list([connected users] * number of users)
                rho: penalty parameter
        '''
        self.D = num_doc 
        self.K = num_topic
        self.V = num_voca
        self.U = num_user

        # alpha is fixed instead of estimated
        self.alpha = .1

        # variational parameter \gamma
        self.gamma = np.random.gamma(100., 1./100, [self.D, self.K])
        # topic word multinomial \beta
        self.beta = np.random.dirichlet([5]*self.V, self.K)

        # Edge parameter
        self.nu = 0
        self.eta = np.random.normal(0.,1, self.K)
        
        # variational parameter phi
        self.phi = list()
        # user interest U * K
        self.pi = np.zeros([self.U, self.K])    

        # Construst the \phi list: a list of K * W (W is the number of unique words)
        for di in xrange(self.D):
            unique_word = len(doc_ids[di])
            cnt = doc_cnt[di]
            self.phi.append(np.random.dirichlet([10]*self.K, unique_word).T)    
        # Construct the initial user interest matrix U * K
        # for each user    
        for ui in xrange(self.U):
            #get the doc list for user ui
            documents_ui = user_ids[ui]
            #storage for user interest vector
            pi_ = np.zeros([self.K])
            #word count
            wc_ = 0
            #for each document in the user documents pool
            for dj in documents_ui: 
                pi_ += np.sum(doc_cnt[dj] * self.phi[dj],1)
                wc_ += np.sum(doc_cnt[dj])
            #store the aggregated user interest
            self.pi[ui,:] = pi_/wc_


        self.user_links = user_links
        self.user_ids = user_ids
        self.doc_ids = doc_ids
        self.doc_cnt = doc_cnt
        self.rho = rho  

        # User to document mapping
        U_D = np.zeros([self.U,self.D])
        for i in range(self.U):
            U_D[i,self.user_ids[i]] = 1

        # Create document to user mapping
        self.doc_user = []
        for i in range(self.D):
            self.doc_user.append(np.where(U_D[:,i] == 1)[0].tolist())

        # Create User overall word count
        self.User_cnt = []
        for ui in range(self.U):
            cnt = 0
            for di in self.user_ids[ui]:
                cnt += np.sum(self.doc_cnt[di])
            self.User_cnt.append(cnt)

        print 'Initialize RAM: num_voca:%d, num_topic:%d, num_doc:%d, num_user:%d' % (self.V,self.K,self.D,self.U)

    def posterior_inference(self, tol):
        error_0 = self.compute_elbo()
        iter_ = 0
        while 1:
            self.variation_update()
            self.parameter_estimation()
            error_1 =  self.compute_elbo()
            # print'%d iter: ELBO = %.3f' % (iter_, error_1)
            iter_ +=1
            if np.abs(error_1 - error_0) <= (tol * np.abs(error_0)):
                print'%d iter: ELBO = %.3f' % (iter_, error_1)
		return 1
            else:
                error_0 = error_1
               
    def posterior_inference1(self, max_iter):
        for iter_ in xrange(max_iter):
            self.variation_update()
            self.parameter_estimation()
            print '%d iter: ELBO = %.3f' % (iter_, self.compute_elbo())
            
    def UpdatePiOnceDocumentIchanged(self,di):
        '''
            Update the pi for users once a document has an updaetd phi
            Parameter:
                di: the document that has an updated phi
        '''
        # it only affects users that have document di
        User_list = self.doc_user[di]
        for A_user in User_list:
            #get the doc list for A_user
            documents_ui = self.user_ids[A_user]
            #storage for user interest vector
            pi_ = np.zeros([self.K])
            #word count
            wc_ = 0
            #for each document in the user documents pool
            for dj in documents_ui: 
                pi_ += np.sum(self.doc_cnt[dj] * self.phi[dj],1)
                wc_ += np.sum(self.doc_cnt[dj])
            #store the aggregated user interest
            self.pi[A_user,:] = pi_/wc_


    def compute_elbo(self):
        """ 
            compute evidence lower bound for trained model
        """
        elbo = 0

        e_log_theta = psi(self.gamma) - psi(np.sum(self.gamma, 1))[:,np.newaxis] # D x K
        log_beta = np.log(self.beta+eps)

        # This is the same elbo for LDA
        for di in xrange(self.D):
            words = self.doc_ids[di]
            cnt = self.doc_cnt[di]
            
            elbo += np.sum(cnt * (self.phi[di] * log_beta[:,words])) # E_q[log p(w_{d,n}|\beta,z_{d,n})]
            elbo += np.sum((self.alpha - 1.)*e_log_theta[di,:]) # E_q[log p(\theta_d | alpha)]
            #Fix of the bug here: missed a cnt
            #Tracer()()
            elbo += np.sum(np.array(cnt)[:,None] * self.phi[di].T * e_log_theta[di,:])  # E_q[log p(z_{d,n}|\theta_d)]

            elbo += -gammaln(np.sum(self.gamma[di,:])) + np.sum(gammaln(self.gamma[di,:])) \
                - np.sum((self.gamma[di,:] - 1.)*(e_log_theta[di,:]))   # - E_q[log q(theta|gamma)]
            elbo += - np.sum(cnt * self.phi[di] * np.log(self.phi[di])) # - E_q[log q(z|phi)]
    
        # The link is at the user level
        # here we double counted the link
        for ui in xrange(self.U):
            for aui in self.user_links[ui]:
                # E_q[log p(y_{u1,u2}|z_{u1},z_{u2},\eta,\nu)]
                elbo += np.dot(self.eta, self.pi[ui]*self.pi[aui]) + self.nu 

        return elbo

    def variation_update(self):
        '''
            Update the variational parameters
        '''
        # update phi, gamma
        e_log_theta = psi(self.gamma) - psi(np.sum(self.gamma, 1))[:,np.newaxis]

        new_beta = np.zeros([self.K, self.V])

        for di in xrange(self.D):
            words = self.doc_ids[di]
            cnt = self.doc_cnt[di]
            doc_len = np.sum(cnt)

            new_phi = np.log(self.beta[:,words]+eps) + e_log_theta[di,:][:,np.newaxis]
            
            # The core parts that differentiate RAM from RTM
            gradient = np.zeros(self.K)
            temp = np.zeros(self.K)

            users_having_di = self.doc_user[di]
            for A_user in users_having_di:
                for aui in self.user_links[A_user]:
                    # if this document is not on the other size of the link
                    # this is same as pooling all the documents into one large document and run RTM
                    if di not in self.user_ids[aui]:
                        gradient += self.eta * self.pi[aui,:] / self.User_cnt[A_user]
                    else:
                        # if this document is on the other size of the link
                        assert di in self.user_ids[aui]
                        temp += self.eta * (self.pi[aui,:] / self.User_cnt[A_user] + \
                        self.pi[A_user,:] / self.User_cnt[aui] )
            
            # becuase of double counting links that have di on both arms
            gradient += temp/2.

            # the rest follows the same as RTM
            new_phi += gradient[:,np.newaxis]
            new_phi = np.exp(new_phi)
            new_phi = new_phi/np.sum(new_phi,0)

            self.phi[di] = new_phi

            # once phi is updated for a document we need to re update \
            # the user (that owns that document) interest
            self.UpdatePiOnceDocumentIchanged(di)

            # gamma update stays the same as RTM
            self.gamma[di,:] = np.sum(cnt * self.phi[di], 1) + self.alpha

            new_beta[:, words] += (cnt * self.phi[di])

        self.beta = new_beta / np.sum(new_beta, 1)[:,np.newaxis]


    def parameter_estimation(self):
        '''
            Update the model parameters: eta, nu
            This is the same as RTM
        '''
        pi_sum = np.zeros(self.K)

        num_links = 0.

        for ui in xrange(self.U):
            for aui in self.user_links[ui]:
                pi_sum += self.pi[ui,:]*self.pi[aui,:]
                num_links += 1

        num_links /= 2. # divide by 2 for bidirectional edge
        pi_sum /= 2.

        pi_alpha = np.zeros(self.K) + self.alpha/(self.alpha*self.K)*self.alpha/(self.alpha*self.K)

        self.nu = np.log(num_links-np.sum(pi_sum)) - np.log(self.rho*(self.K-1)/self.K + num_links - np.sum(pi_sum))
        self.eta = np.log(pi_sum) - np.log(pi_sum + self.rho * pi_alpha) - self.nu 

    def save_model(self, output_directory, vocab=None):
        import os
        if not os.path.exists(output_directory):
            os.mkdir(output_directory)

        np.savetxt(output_directory+'/eta.txt', self.eta, delimiter='\t')
        np.savetxt(output_directory+'/beta.txt', self.beta, delimiter='\t')
        np.savetxt(output_directory+'/gamma.txt',self.gamma,delimiter='\t')
        with open(output_directory+'/nu.txt', 'w') as f:
            f.write('%f\n'%self.nu)

        if vocab != None:
            #NO utils
            #utils.write_top_words(self.beta, vocab, output_directory+'/top_words.csv')
            pass
